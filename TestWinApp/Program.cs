﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Collections;


namespace TestWinApp
{
    static class Program
    {
        public static String fileName = "";
        public static int sizeQuestion = 0;

        public static List<Question> questions = new List<Question>();

        public static int markA = 0;
        public static int markB = 0;
        public static int markC = 0; 
        public static int count;

        public static void markNullable() {
            markA = 0;
            markB = 0;
            markC = 0;
        }

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
