﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestWinApp
{
    class Question
    {
        private int id;
        private String question;
        private String[] answers;
        private bool[] selected;

        public static int count = 0;
        public Question(String question)
        {
            this.question = question;
        }
        public Question(String question, String[] answer, bool[] selected)
        {
            count++;
            this.id = count;
            this.question = question;
            this.answers = answer;
            this.selected = selected;

        }

        public static int getCount()
        {
            return Question.count;
        }

        public String getQuestion()
        {
            return this.question;
        }

        public void setQuestion(String quest)
        {
            this.question = quest;
        }

        public String[] getAnswers() {
            return this.answers;            
        }

        public void setAnswers(String[] answer)
        {
            this.answers[0] = answer[0];
            this.answers[1] = answer[1];
            this.answers[2] = answer[2];
        }

        public bool[] getSelected() {
            return this.selected;
        }

        public void setSelected(bool[] selected) {
            this.selected = selected;
        }
    }
}
