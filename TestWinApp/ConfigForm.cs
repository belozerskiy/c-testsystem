﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace TestWinApp
{
    public partial class ConfigForm : Form
    {
        public ConfigForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            if (fiveTextBox.Text != null && threeTextBox.Text != null && foureTextBox.Text != null) {
                Program.markA = int.Parse(fiveTextBox.Text);
                Program.markB = int.Parse(foureTextBox.Text);
                Program.markC = int.Parse(threeTextBox.Text);
                Program.count = int.Parse(questionsTextBox.Text);

                if (Program.markA > Program.markB && Program.markB > Program.markC)
                {
                    StreamWriter writer = new StreamWriter(Program.fileName);
                    writer.WriteLine("Settings [");
                    writer.WriteLine("mark-a:" + Program.markA + ",");
                    writer.WriteLine("mark-b:" + Program.markB + ",");
                    writer.WriteLine("mark-c:" + Program.markC + ",");
                    writer.WriteLine("count:" + Program.count);
                    writer.WriteLine("]\n");
                    writer.Close();
                }
                else 
                {
                    Program.markNullable();
                    MessageBox.Show("No data valid: 5 > 4 > 3", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            button2_Click(sender, e);
        }

        private void ConfigForm_Load(object sender, EventArgs e)
        {
            fiveTextBox.Text = Program.markA.ToString();
            foureTextBox.Text = Program.markB.ToString();
            threeTextBox.Text = Program.markC.ToString();
            questionsTextBox.Text = Program.count.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
