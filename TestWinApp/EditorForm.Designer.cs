﻿namespace TestWinApp
{
    partial class EditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.answerBox3 = new System.Windows.Forms.TextBox();
            this.answerCheckBox3 = new System.Windows.Forms.CheckBox();
            this.answerBox2 = new System.Windows.Forms.TextBox();
            this.answerCheckBox2 = new System.Windows.Forms.CheckBox();
            this.answerBox1 = new System.Windows.Forms.TextBox();
            this.answerCheckBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.questionBox = new System.Windows.Forms.TextBox();
            this.addBtn = new System.Windows.Forms.Button();
            this.delBtn = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.AddButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.delButton = new System.Windows.Forms.Button();
            this.OkBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.answerBox3);
            this.groupBox1.Controls.Add(this.answerCheckBox3);
            this.groupBox1.Controls.Add(this.answerBox2);
            this.groupBox1.Controls.Add(this.answerCheckBox2);
            this.groupBox1.Controls.Add(this.answerBox1);
            this.groupBox1.Controls.Add(this.answerCheckBox1);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox1.Location = new System.Drawing.Point(12, 107);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Anwer";
            // 
            // answerBox3
            // 
            this.answerBox3.Location = new System.Drawing.Point(28, 69);
            this.answerBox3.Name = "answerBox3";
            this.answerBox3.Size = new System.Drawing.Size(226, 20);
            this.answerBox3.TabIndex = 1;
            // 
            // answerCheckBox3
            // 
            this.answerCheckBox3.AutoSize = true;
            this.answerCheckBox3.Location = new System.Drawing.Point(7, 72);
            this.answerCheckBox3.Name = "answerCheckBox3";
            this.answerCheckBox3.Size = new System.Drawing.Size(15, 14);
            this.answerCheckBox3.TabIndex = 0;
            this.answerCheckBox3.UseVisualStyleBackColor = true;
            // 
            // answerBox2
            // 
            this.answerBox2.Location = new System.Drawing.Point(28, 43);
            this.answerBox2.Name = "answerBox2";
            this.answerBox2.Size = new System.Drawing.Size(226, 20);
            this.answerBox2.TabIndex = 1;
            // 
            // answerCheckBox2
            // 
            this.answerCheckBox2.AutoSize = true;
            this.answerCheckBox2.Location = new System.Drawing.Point(7, 46);
            this.answerCheckBox2.Name = "answerCheckBox2";
            this.answerCheckBox2.Size = new System.Drawing.Size(15, 14);
            this.answerCheckBox2.TabIndex = 0;
            this.answerCheckBox2.UseVisualStyleBackColor = true;
            // 
            // answerBox1
            // 
            this.answerBox1.Location = new System.Drawing.Point(28, 17);
            this.answerBox1.Name = "answerBox1";
            this.answerBox1.Size = new System.Drawing.Size(226, 20);
            this.answerBox1.TabIndex = 1;
            // 
            // answerCheckBox1
            // 
            this.answerCheckBox1.AutoSize = true;
            this.answerCheckBox1.Location = new System.Drawing.Point(7, 20);
            this.answerCheckBox1.Name = "answerCheckBox1";
            this.answerCheckBox1.Size = new System.Drawing.Size(15, 14);
            this.answerCheckBox1.TabIndex = 0;
            this.answerCheckBox1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.questionBox);
            this.groupBox2.Location = new System.Drawing.Point(12, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(260, 80);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Question";
            // 
            // questionBox
            // 
            this.questionBox.Location = new System.Drawing.Point(6, 19);
            this.questionBox.Multiline = true;
            this.questionBox.Name = "questionBox";
            this.questionBox.Size = new System.Drawing.Size(248, 54);
            this.questionBox.TabIndex = 0;
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(199, 209);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(37, 23);
            this.addBtn.TabIndex = 2;
            this.addBtn.Text = "+";
            this.addBtn.UseVisualStyleBackColor = true;
            // 
            // delBtn
            // 
            this.delBtn.Location = new System.Drawing.Point(236, 209);
            this.delBtn.Name = "delBtn";
            this.delBtn.Size = new System.Drawing.Size(37, 23);
            this.delBtn.TabIndex = 2;
            this.delBtn.Text = "-";
            this.delBtn.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(278, 21);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(371, 212);
            this.listBox1.TabIndex = 3;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(278, 236);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(75, 23);
            this.AddButton.TabIndex = 4;
            this.AddButton.Text = "Add";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Enabled = false;
            this.saveButton.Location = new System.Drawing.Point(359, 236);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 4;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // delButton
            // 
            this.delButton.Enabled = false;
            this.delButton.Location = new System.Drawing.Point(440, 236);
            this.delButton.Name = "delButton";
            this.delButton.Size = new System.Drawing.Size(75, 23);
            this.delButton.TabIndex = 4;
            this.delButton.Text = "Delete";
            this.delButton.UseVisualStyleBackColor = true;
            this.delButton.Click += new System.EventHandler(this.delButton_Click);
            // 
            // OkBtn
            // 
            this.OkBtn.Location = new System.Drawing.Point(493, 312);
            this.OkBtn.Name = "OkBtn";
            this.OkBtn.Size = new System.Drawing.Size(75, 23);
            this.OkBtn.TabIndex = 5;
            this.OkBtn.Text = "OK";
            this.OkBtn.UseVisualStyleBackColor = true;
            this.OkBtn.Click += new System.EventHandler(this.OkBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(574, 312);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 5;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // EditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 347);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.OkBtn);
            this.Controls.Add(this.delButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.delBtn);
            this.Controls.Add(this.addBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "EditorForm";
            this.Text = "Editor";
            this.Load += new System.EventHandler(this.EditorForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox answerBox3;
        private System.Windows.Forms.CheckBox answerCheckBox3;
        private System.Windows.Forms.TextBox answerBox2;
        private System.Windows.Forms.CheckBox answerCheckBox2;
        private System.Windows.Forms.TextBox answerBox1;
        private System.Windows.Forms.CheckBox answerCheckBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox questionBox;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Button delBtn;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button delButton;
        private System.Windows.Forms.Button OkBtn;
        private System.Windows.Forms.Button cancelBtn;
    }
}