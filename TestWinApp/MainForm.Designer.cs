﻿namespace TestWinApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.resBtn = new System.Windows.Forms.Button();
            this.opnBtn = new System.Windows.Forms.Button();
            this.newBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.startBtn = new System.Windows.Forms.Button();
            this.editBtn = new System.Windows.Forms.Button();
            this.cfgBtn = new System.Windows.Forms.Button();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.resBtn);
            this.groupBox1.Controls.Add(this.opnBtn);
            this.groupBox1.Controls.Add(this.newBtn);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(90, 110);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Menu";
            // 
            // resBtn
            // 
            this.resBtn.Location = new System.Drawing.Point(7, 80);
            this.resBtn.Name = "resBtn";
            this.resBtn.Size = new System.Drawing.Size(75, 23);
            this.resBtn.TabIndex = 2;
            this.resBtn.Text = "Results";
            this.resBtn.UseVisualStyleBackColor = true;
            // 
            // opnBtn
            // 
            this.opnBtn.Location = new System.Drawing.Point(7, 50);
            this.opnBtn.Name = "opnBtn";
            this.opnBtn.Size = new System.Drawing.Size(75, 23);
            this.opnBtn.TabIndex = 1;
            this.opnBtn.Tag = "open";
            this.opnBtn.Text = "Open";
            this.opnBtn.UseVisualStyleBackColor = true;
            this.opnBtn.Click += new System.EventHandler(this.Select_Form);
            // 
            // newBtn
            // 
            this.newBtn.Location = new System.Drawing.Point(7, 20);
            this.newBtn.Name = "newBtn";
            this.newBtn.Size = new System.Drawing.Size(75, 23);
            this.newBtn.TabIndex = 0;
            this.newBtn.Tag = "new";
            this.newBtn.Text = "New";
            this.newBtn.UseVisualStyleBackColor = true;
            this.newBtn.Click += new System.EventHandler(this.Select_Form);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.startBtn);
            this.groupBox2.Controls.Add(this.editBtn);
            this.groupBox2.Controls.Add(this.cfgBtn);
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(110, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(88, 110);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Settings";
            // 
            // startBtn
            // 
            this.startBtn.Location = new System.Drawing.Point(6, 78);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(75, 23);
            this.startBtn.TabIndex = 2;
            this.startBtn.Tag = "start";
            this.startBtn.Text = "start";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.Select_Form);
            // 
            // editBtn
            // 
            this.editBtn.Location = new System.Drawing.Point(6, 49);
            this.editBtn.Name = "editBtn";
            this.editBtn.Size = new System.Drawing.Size(75, 23);
            this.editBtn.TabIndex = 1;
            this.editBtn.Tag = "editor";
            this.editBtn.Text = "editor";
            this.editBtn.UseVisualStyleBackColor = true;
            this.editBtn.Click += new System.EventHandler(this.Select_Form);
            // 
            // cfgBtn
            // 
            this.cfgBtn.Location = new System.Drawing.Point(7, 20);
            this.cfgBtn.Name = "cfgBtn";
            this.cfgBtn.Size = new System.Drawing.Size(75, 23);
            this.cfgBtn.TabIndex = 0;
            this.cfgBtn.Tag = "config";
            this.cfgBtn.Text = "config";
            this.cfgBtn.UseVisualStyleBackColor = true;
            this.cfgBtn.Click += new System.EventHandler(this.Select_Form);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.FileName = "*.txt";
            this.saveFileDialog.Filter = "files|*.txt";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "*.txt";
            this.openFileDialog.Filter = "files|*.txt";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(211, 185);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "MainForm";
            this.Text = "Test";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button resBtn;
        private System.Windows.Forms.Button opnBtn;
        private System.Windows.Forms.Button newBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.Button editBtn;
        private System.Windows.Forms.Button cfgBtn;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;

    }
}

