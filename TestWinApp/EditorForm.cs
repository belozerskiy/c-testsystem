﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace TestWinApp
{
    public partial class EditorForm : Form
    {

        Question quest = null;
        String[] answers = null;
        bool[] selected = null;

        public EditorForm()
        {
            InitializeComponent();
        }

        private void editNullable() {
            questionBox.Text = "";
            answerBox1.Text = answerBox2.Text = answerBox3.Text = "";
            answerCheckBox1.Checked = answerCheckBox2.Checked = answerCheckBox3.Checked = false;
            listBox1.Items.Clear();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            if (questionBox.Text != "")
            {
                quest = new Question(questionBox.Text, new String[] { answerBox1.Text, answerBox2.Text, answerBox3.Text },  
                    new bool[]{answerCheckBox1.Checked,answerCheckBox2.Checked, answerCheckBox3.Checked});
                Program.questions.Add(quest);
                listBox1.Items.Add(quest.getQuestion());
                this.editNullable();
            }
            else 
            {
                MessageBox.Show("Few data in textbox", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void delButton_Click(object sender, EventArgs e)
        {
            Program.questions.Remove(Program.questions[listBox1.SelectedIndex]);
            listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            this.editNullable();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                delButton.Enabled = true;
                saveButton.Enabled = true;
                quest = Program.questions[listBox1.SelectedIndex];
                questionBox.Text = quest.getQuestion();

                answers = quest.getAnswers();
                answerBox1.Text = answers[0];
                answerBox2.Text = answers[1];
                answerBox3.Text = answers[2];

                selected = quest.getSelected();
                answerCheckBox1.Checked = selected[0];
                answerCheckBox2.Checked = selected[1];
                answerCheckBox3.Checked = selected[2];
            }
            else 
            {
                delButton.Enabled = false;
                saveButton.Enabled = false;
                this.editNullable();

            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (questionBox.Text != "")
            {
                quest.setQuestion(questionBox.Text);
                quest.setAnswers(new String[] { answerBox1.Text, answerBox2.Text, answerBox3.Text });
                quest.setSelected(new bool[] { answerCheckBox1.Checked, answerCheckBox2.Checked, answerCheckBox3.Checked});

                listBox1.Items[listBox1.SelectedIndex] = quest.getQuestion();
                answers = quest.getAnswers();
                answerBox1.Text = answers[0];
                answerBox2.Text = answers[1];
                answerBox3.Text = answers[2];
                bool[] selected = quest.getSelected();
                answerCheckBox1.Checked = selected[0];
                answerCheckBox2.Checked = selected[1];
                answerCheckBox3.Checked = selected[2];
            }
            else
            {
                MessageBox.Show("Few data in textbox", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            StreamWriter writer = new StreamWriter(Program.fileName);

            writer.WriteLine("Settings [");
            writer.WriteLine("mark-a:" + Program.markA + ",");
            writer.WriteLine("mark-b:" + Program.markB + ",");
            writer.WriteLine("mark-c:" + Program.markC + ",");
            writer.WriteLine("count:" + Program.count);
            writer.WriteLine("]\n");

            writer.WriteLine("Questions [");
            
            foreach(Question question in Program.questions)
            {
                writer.Write("question:" + question.getQuestion() + ":");
                String[] str = question.getAnswers();
                for (int i = 0; i < str.Length; i++) {
                    if(i < str.Length - 1) { 
                        writer.Write(str[i] + ",");
                    }
                    else
                    {
                        writer.Write(str[i]);
                    }
                }
                writer.Write(":");

                bool[] selected = question.getSelected();
                for (int i = 0; i < selected.Length; i++)
                {
                    if (i < str.Length - 1)
                    {
                        writer.Write(selected[i] + ",");
                    }
                    else
                    {
                        writer.Write(selected[i]);
                    }
                }
                writer.WriteLine();
            }
            writer.WriteLine("]\n");
            writer.Close();
            this.Close();
        }

        private void EditorForm_Load(object sender, EventArgs e)
        {
            Program.questions.Clear();
            editNullable();

            StreamReader reader = new StreamReader(Program.fileName);
            String line = null;
            while ((line = reader.ReadLine()) != null)
            {
                if (line.Contains("question"))
                {
                    int firstSepIdx = line.IndexOf(":");
                    line = line.Substring(firstSepIdx);
                    int secondSepIdx = line.IndexOf(":") + 1;

                    String[] str = line.Substring(secondSepIdx).Split(':');
                    String[] answ = str[1].Split(',');
                    bool[] sel = new bool[] { Boolean.Parse(str[2].Split(',')[0]), Boolean.Parse(str[2].Split(',')[1]), Boolean.Parse(str[2].Split(',')[2]) };
                    quest = new Question(str[0], answ, sel);

                    Program.questions.Add(quest);
                    listBox1.Items.Add(quest.getQuestion());
                }
            }
            reader.Close();   
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Are you sure?", "Sure", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (res == DialogResult.Cancel)
            {
                EditorForm_Load(sender, e);
                return;
            }
            this.Close();
        }
    }
}
