﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace TestWinApp
{
    public partial class MainForm : Form
    {       
        public MainForm()
        {
            InitializeComponent();
        }

        private void Select_Form(object sender, EventArgs e)
        {
            String tag = ((Button)sender).Tag.ToString();
            
            if (tag == "new")
            {
                DialogResult result = this.saveFileDialog.ShowDialog();
                Program.fileName = this.saveFileDialog.FileName;
                if (result == DialogResult.OK) { 
                    StreamWriter writer = new StreamWriter(Program.fileName);
                    writer.Close();
                    this.groupBox2.Enabled = true;
                }
                else if(result == DialogResult.Cancel)
                {
                    DialogResult res = MessageBox.Show("Are you sure?", "Sure", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (res == DialogResult.Cancel)
                    {
                        Select_Form(sender, e);
                    }
                }
                else 
                {
                    MessageBox.Show("Error");                     
                }
                Program.markNullable();
                return;
            }
            else if (tag == "open")
            {
                DialogResult result = this.openFileDialog.ShowDialog();
                Program.fileName = this.openFileDialog.FileName;
                if (result.ToString().Equals("OK"))
                {
                    StreamReader reader = new StreamReader(Program.fileName);
                    String line = null;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Contains("mark-a"))
                        {
                            Program.markA = int.Parse(line.Substring(line.IndexOf(":") + 1, line.IndexOf(",") - 1 - line.IndexOf(":")));
                        }
                        else if (line.Contains("mark-b"))
                        {
                            Program.markB = int.Parse(line.Substring(line.IndexOf(":") + 1, line.IndexOf(",") - 1 - line.IndexOf(":")));
                        }
                        else if (line.Contains("mark-c"))
                        {
                            Program.markC = int.Parse(line.Substring(line.IndexOf(":") + 1, line.IndexOf(",") - 1 - line.IndexOf(":")));
                        }
                        else if (line.Contains("count-questions"))
                        {
                            Program.count = int.Parse(line.Substring(line.IndexOf(":") + 1, line.Length - 1 - line.IndexOf(":")));
                        }
                        Console.WriteLine(line);
                    }
                    reader.Close();
                    this.groupBox2.Enabled = true;
                }
                else if (result == DialogResult.Cancel)
                {
                    DialogResult res = MessageBox.Show("Are you sure?", "Sure", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (res == DialogResult.Cancel)
                    {
                        Select_Form(sender, e);
                    }
                    this.groupBox2.Enabled = false;
                }
                else
                {
                    MessageBox.Show("Error");
                    this.groupBox2.Enabled = false;
                }
                Program.markNullable();
                return;
            }

            Form form = null;
       
            if (tag == "config")
            {
                form = new ConfigForm();
            }else if(tag == "editor")
            {
                form = new EditorForm();
            }else if (tag == "start")
            {
                form = new StartForm();
            }
            form.Show();
        }
    }
}

